<?php
/**
 * @file
 * Contains the default views for the tripal api site
 */

/**
 * Implements hook_views_default_views().
 */
function tripal_api_site_views_default_views() {
  $views = array();

  $view = tripal_api_site_defaultvalue_admin_topics();
  $views[$view->name] = $view;

  $view = tripal_api_site_defaultvalue_admin_search();
  $views[$view->name] = $view;

  return $views;
}

/**
 * Lists Topics
 * Note: the Drupal & Views Topics are pages
 */
function tripal_api_site_defaultvalue_admin_topics() {

$view = new view();
$view->name = 'tripal_topics';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'api_documentation';
$view->human_name = 'Tripal Topics';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Views';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'object_name' => 'object_name',
);
$handler->display->display_options['style_options']['default'] = 'object_name';
$handler->display->display_options['style_options']['info'] = array(
  'object_name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Set Session';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = '<?php
  $path = current_path();
  if (preg_match(\'/tripal-api\\/current\\/topics/\', $path)) {
    // DO NOT SET
  }
  elseif (preg_match(\'/tripal-api\\/1.x\\/topics/\', $path)) {
      tripal_api_site_set_version(1);
  }
  else {
      tripal_api_site_set_version(2);
  }
  ?>';
$handler->display->display_options['header']['area']['format'] = 'php_code';
/* Field: API documentation: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'api_documentation';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Name';
$handler->display->display_options['fields']['title']['link_to_docs'] = 'object';
/* Field: API documentation: Summary */
$handler->display->display_options['fields']['summary']['id'] = 'summary';
$handler->display->display_options['fields']['summary']['table'] = 'api_documentation';
$handler->display->display_options['fields']['summary']['field'] = 'summary';
$handler->display->display_options['fields']['summary']['label'] = 'Description';
/* Sort criterion: API documentation: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'api_documentation';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Contextual filter: API documentation: Branch name */
$handler->display->display_options['arguments']['branch_name']['id'] = 'branch_name';
$handler->display->display_options['arguments']['branch_name']['table'] = 'api_branch';
$handler->display->display_options['arguments']['branch_name']['field'] = 'branch_name';
$handler->display->display_options['arguments']['branch_name']['relationship'] = 'branch_id';
$handler->display->display_options['arguments']['branch_name']['default_action'] = 'default';
$handler->display->display_options['arguments']['branch_name']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['branch_name']['default_argument_options']['code'] = '$_SESSION[\'api_tripal\'];';
$handler->display->display_options['arguments']['branch_name']['default_argument_skip_url'] = TRUE;
$handler->display->display_options['arguments']['branch_name']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['branch_name']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['branch_name']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['branch_name']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['branch_name']['validate']['type'] = 'php';
$handler->display->display_options['arguments']['branch_name']['validate_options']['code'] = 'if($argument == \'current\') {
    $handler->argument = $_SESSION[\'api_views\'];
    return TRUE;
  } elseif ($argument == \'3.x\' OR $argument == \'3.x\') {
    return TRUE;
  }
  else {
    return FALSE;
  }
  ';
$handler->display->display_options['arguments']['branch_name']['validate']['fail'] = 'empty';
$handler->display->display_options['arguments']['branch_name']['limit'] = '0';
/* Filter criterion: API documentation: Class documentation ID */
$handler->display->display_options['filters']['class_did']['id'] = 'class_did';
$handler->display->display_options['filters']['class_did']['table'] = 'api_documentation';
$handler->display->display_options['filters']['class_did']['field'] = 'class_did';
$handler->display->display_options['filters']['class_did']['value']['value'] = '0';
$handler->display->display_options['filters']['class_did']['group'] = 1;
/* Filter criterion: API documentation: Object type */
$handler->display->display_options['filters']['object_type']['id'] = 'object_type';
$handler->display->display_options['filters']['object_type']['table'] = 'api_documentation';
$handler->display->display_options['filters']['object_type']['field'] = 'object_type';
$handler->display->display_options['filters']['object_type']['value'] = 'group';
$handler->display->display_options['filters']['object_type']['group'] = 1;
/* Filter criterion: API documentation: Title */
$handler->display->display_options['filters']['title_1']['id'] = 'title_1';
$handler->display->display_options['filters']['title_1']['table'] = 'api_documentation';
$handler->display->display_options['filters']['title_1']['field'] = 'title';
$handler->display->display_options['filters']['title_1']['operator'] = 'word';
$handler->display->display_options['filters']['title_1']['group'] = 1;
$handler->display->display_options['filters']['title_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['title_1']['expose']['operator_id'] = 'title_1_op';
$handler->display->display_options['filters']['title_1']['expose']['label'] = 'Name';
$handler->display->display_options['filters']['title_1']['expose']['operator'] = 'title_1_op';
$handler->display->display_options['filters']['title_1']['expose']['identifier'] = 'title_1';
$handler->display->display_options['filters']['title_1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
/* Filter criterion: API documentation: Summary */
$handler->display->display_options['filters']['summary']['id'] = 'summary';
$handler->display->display_options['filters']['summary']['table'] = 'api_documentation';
$handler->display->display_options['filters']['summary']['field'] = 'summary';
$handler->display->display_options['filters']['summary']['operator'] = 'word';
$handler->display->display_options['filters']['summary']['group'] = 1;
$handler->display->display_options['filters']['summary']['exposed'] = TRUE;
$handler->display->display_options['filters']['summary']['expose']['operator_id'] = 'summary_op';
$handler->display->display_options['filters']['summary']['expose']['label'] = 'Description';
$handler->display->display_options['filters']['summary']['expose']['operator'] = 'summary_op';
$handler->display->display_options['filters']['summary']['expose']['identifier'] = 'summary';
$handler->display->display_options['filters']['summary']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);

/* Display: API */
$handler = $view->new_display('page', 'API', 'page');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Tripal API';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: API documentation: Branch */
$handler->display->display_options['arguments']['branch_id']['id'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['table'] = 'api_documentation';
$handler->display->display_options['arguments']['branch_id']['field'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['branch_id']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['branch_id']['title'] = 'Tripal API';
$handler->display->display_options['arguments']['branch_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['branch_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['branch_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['branch_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['branch_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['branch_id']['validate']['type'] = 'php';
$handler->display->display_options['arguments']['branch_id']['validate_options']['code'] = 'if ($argument == \'current\') {
  $version = tripal_api_site_get_version(\'tripal\');
}
else {
  $version = $argument;
}
$handler->argument = tripal_api_site_get_branch_id(\'tripal\', $version);
return TRUE;';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: API documentation: Class documentation ID */
$handler->display->display_options['filters']['class_did']['id'] = 'class_did';
$handler->display->display_options['filters']['class_did']['table'] = 'api_documentation';
$handler->display->display_options['filters']['class_did']['field'] = 'class_did';
$handler->display->display_options['filters']['class_did']['value']['value'] = '0';
$handler->display->display_options['filters']['class_did']['group'] = 1;
/* Filter criterion: API documentation: Object type */
$handler->display->display_options['filters']['object_type']['id'] = 'object_type';
$handler->display->display_options['filters']['object_type']['table'] = 'api_documentation';
$handler->display->display_options['filters']['object_type']['field'] = 'object_type';
$handler->display->display_options['filters']['object_type']['value'] = 'group';
$handler->display->display_options['filters']['object_type']['group'] = 1;
/* Filter criterion: API documentation: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'api_documentation';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'word';
$handler->display->display_options['filters']['title']['value'] = 'api';
$handler->display->display_options['filters']['title']['group'] = 1;
/* Filter criterion: API documentation: Title */
$handler->display->display_options['filters']['title_1']['id'] = 'title_1';
$handler->display->display_options['filters']['title_1']['table'] = 'api_documentation';
$handler->display->display_options['filters']['title_1']['field'] = 'title';
$handler->display->display_options['filters']['title_1']['operator'] = 'word';
$handler->display->display_options['filters']['title_1']['group'] = 1;
$handler->display->display_options['filters']['title_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['title_1']['expose']['operator_id'] = 'title_1_op';
$handler->display->display_options['filters']['title_1']['expose']['label'] = 'Name';
$handler->display->display_options['filters']['title_1']['expose']['operator'] = 'title_1_op';
$handler->display->display_options['filters']['title_1']['expose']['identifier'] = 'title_1';
$handler->display->display_options['filters']['title_1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
/* Filter criterion: API documentation: Summary */
$handler->display->display_options['filters']['summary']['id'] = 'summary';
$handler->display->display_options['filters']['summary']['table'] = 'api_documentation';
$handler->display->display_options['filters']['summary']['field'] = 'summary';
$handler->display->display_options['filters']['summary']['operator'] = 'word';
$handler->display->display_options['filters']['summary']['group'] = 1;
$handler->display->display_options['filters']['summary']['exposed'] = TRUE;
$handler->display->display_options['filters']['summary']['expose']['operator_id'] = 'summary_op';
$handler->display->display_options['filters']['summary']['expose']['label'] = 'Description';
$handler->display->display_options['filters']['summary']['expose']['operator'] = 'summary_op';
$handler->display->display_options['filters']['summary']['expose']['identifier'] = 'summary';
$handler->display->display_options['filters']['summary']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
$handler->display->display_options['path'] = 'tripal-api/%/topics';
$handler->display->display_options['menu']['title'] = 'Tripal API';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Modules */
$handler = $view->new_display('page', 'Modules', 'page_1');
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: API documentation: Branch */
$handler->display->display_options['arguments']['branch_id']['id'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['table'] = 'api_documentation';
$handler->display->display_options['arguments']['branch_id']['field'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['branch_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['branch_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['branch_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['branch_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['branch_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['branch_id']['validate']['type'] = 'php';
$handler->display->display_options['arguments']['branch_id']['validate_options']['code'] = 'if ($argument == \'current\') {
  $version = tripal_api_site_get_version(\'tripal\');
}
else {
  $version = $argument;
}
$handler->argument = tripal_api_site_get_branch_id(\'tripal\', $version);
return TRUE;';
$handler->display->display_options['path'] = 'tripal-modules/%/topics';
$handler->display->display_options['menu']['title'] = 'Tripal Modules';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

  return $view;

}

/**
 * Search Views
 */
function tripal_api_site_defaultvalue_admin_search() {

  $view = new view();
$view = new view();
$view->name = 'tripal_search';
$view->description = 'Blocks for searching API documentation. Used directly by the API module. Do not delete or disable.';
$view->tag = 'default';
$view->base_table = 'api_documentation';
$view->human_name = 'Tripal Search';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access API reference';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'input_required';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
$handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'plain_text';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'object_type' => 'object_type',
  'file_name' => 'file_name',
  'summary' => 'summary',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'object_type' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'file_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'summary' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* No results behavior: Global: Unfiltered text */
$handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
$handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
$handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
$handler->display->display_options['empty']['area_text_custom']['label'] = 'No matches';
$handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
$handler->display->display_options['empty']['area_text_custom']['content'] = 'No search results found.';
/* Field: API documentation: Deprecated */
$handler->display->display_options['fields']['deprecated']['id'] = 'deprecated';
$handler->display->display_options['fields']['deprecated']['table'] = 'api_documentation';
$handler->display->display_options['fields']['deprecated']['field'] = 'deprecated';
$handler->display->display_options['fields']['deprecated']['label'] = '';
$handler->display->display_options['fields']['deprecated']['exclude'] = TRUE;
$handler->display->display_options['fields']['deprecated']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['deprecated']['alter']['text'] = 'Deprecated';
$handler->display->display_options['fields']['deprecated']['element_label_colon'] = FALSE;
/* Field: API documentation: Object type */
$handler->display->display_options['fields']['object_type']['id'] = 'object_type';
$handler->display->display_options['fields']['object_type']['table'] = 'api_documentation';
$handler->display->display_options['fields']['object_type']['field'] = 'object_type';
$handler->display->display_options['fields']['object_type']['label'] = 'Type';
$handler->display->display_options['fields']['object_type']['element_class'] = '[deprecated]';
/* Field: API documentation: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'api_documentation';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Name';
$handler->display->display_options['fields']['title']['element_class'] = '[deprecated]';
$handler->display->display_options['fields']['title']['link_to_docs'] = 'object';
/* Field: API documentation: Summary */
$handler->display->display_options['fields']['summary']['id'] = 'summary';
$handler->display->display_options['fields']['summary']['table'] = 'api_documentation';
$handler->display->display_options['fields']['summary']['field'] = 'summary';
$handler->display->display_options['fields']['summary']['label'] = 'Description';
$handler->display->display_options['fields']['summary']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['summary']['alter']['text'] = '<span class=[deprecated]>[deprecated]</span> [summary]';
$handler->display->display_options['fields']['summary']['element_class'] = '[deprecated]';
$handler->display->display_options['fields']['summary']['hide_alter_empty'] = FALSE;
/* Sort criterion: API documentation: Title: length */
$handler->display->display_options['sorts']['title_length']['id'] = 'title_length';
$handler->display->display_options['sorts']['title_length']['table'] = 'api_documentation';
$handler->display->display_options['sorts']['title_length']['field'] = 'title_length';
$handler->display->display_options['sorts']['title_length']['exposed'] = TRUE;
$handler->display->display_options['sorts']['title_length']['expose']['label'] = 'Length of Name';
/* Sort criterion: API documentation: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'api_documentation';
$handler->display->display_options['sorts']['title']['field'] = 'title';
$handler->display->display_options['sorts']['title']['exposed'] = TRUE;
$handler->display->display_options['sorts']['title']['expose']['label'] = 'Name';
/* Sort criterion: API documentation: Object type */
$handler->display->display_options['sorts']['object_type']['id'] = 'object_type';
$handler->display->display_options['sorts']['object_type']['table'] = 'api_documentation';
$handler->display->display_options['sorts']['object_type']['field'] = 'object_type';
$handler->display->display_options['sorts']['object_type']['exposed'] = TRUE;
$handler->display->display_options['sorts']['object_type']['expose']['label'] = 'Type';
/* Sort criterion: API documentation: File name */
$handler->display->display_options['sorts']['file_name']['id'] = 'file_name';
$handler->display->display_options['sorts']['file_name']['table'] = 'api_documentation';
$handler->display->display_options['sorts']['file_name']['field'] = 'file_name';
$handler->display->display_options['sorts']['file_name']['exposed'] = TRUE;
$handler->display->display_options['sorts']['file_name']['expose']['label'] = 'Location';
/* Contextual filter: API documentation: Branch */
$handler->display->display_options['arguments']['branch_id']['id'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['table'] = 'api_documentation';
$handler->display->display_options['arguments']['branch_id']['field'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['branch_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['branch_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['branch_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['branch_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['branch_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['branch_id']['validate']['type'] = 'php';
$handler->display->display_options['arguments']['branch_id']['validate_options']['code'] = 'if ($argument == \'current\') {
  $version = tripal_api_site_get_version(\'tripal\');
}
else {
  $version = $argument;
}
$handler->argument = tripal_api_site_get_branch_id(\'tripal\', $version);
return TRUE;';
/* Filter criterion: API documentation: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'api_documentation';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['group'] = 1;
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Function, file, or topic';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'keyword';
$handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
/* Filter criterion: API documentation: Object type */
$handler->display->display_options['filters']['object_type']['id'] = 'object_type';
$handler->display->display_options['filters']['object_type']['table'] = 'api_documentation';
$handler->display->display_options['filters']['object_type']['field'] = 'object_type';
$handler->display->display_options['filters']['object_type']['group'] = 1;
$handler->display->display_options['filters']['object_type']['exposed'] = TRUE;
$handler->display->display_options['filters']['object_type']['expose']['operator_id'] = 'object_type_op';
$handler->display->display_options['filters']['object_type']['expose']['label'] = 'Filter by Type';
$handler->display->display_options['filters']['object_type']['expose']['operator'] = 'object_type_op';
$handler->display->display_options['filters']['object_type']['expose']['identifier'] = 'object_type';
$handler->display->display_options['filters']['object_type']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);

/* Display: Tripal API */
$handler = $view->new_display('page', 'Tripal API', 'page_1');
$handler->display->display_options['path'] = 'tripal-api/%/search';
$handler->display->display_options['menu']['title'] = 'Search';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Tripal Modules */
$handler = $view->new_display('page', 'Tripal Modules', 'page_4');
$handler->display->display_options['path'] = 'tripal-modules/%/search';
$handler->display->display_options['menu']['title'] = 'Search';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Drupal */
$handler = $view->new_display('page', 'Drupal', 'page_2');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: API documentation: Branch */
$handler->display->display_options['arguments']['branch_id']['id'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['table'] = 'api_documentation';
$handler->display->display_options['arguments']['branch_id']['field'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['branch_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['branch_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['branch_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['branch_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['branch_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['branch_id']['validate']['type'] = 'php';
$handler->display->display_options['arguments']['branch_id']['validate_options']['code'] = 'if ($argument == \'current\') {
  $version = tripal_api_site_get_version(\'drupal\');
}
else {
  $version = $argument;
}
$handler->argument = tripal_api_site_get_branch_id(\'drupal\', $version);
return TRUE;';
$handler->display->display_options['path'] = 'drupal/%/search';

/* Display: Views */
$handler = $view->new_display('page', 'Views', 'page_3');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: API documentation: Branch */
$handler->display->display_options['arguments']['branch_id']['id'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['table'] = 'api_documentation';
$handler->display->display_options['arguments']['branch_id']['field'] = 'branch_id';
$handler->display->display_options['arguments']['branch_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['branch_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['branch_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['branch_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['branch_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['branch_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['branch_id']['validate']['type'] = 'php';
$handler->display->display_options['arguments']['branch_id']['validate_options']['code'] = 'if ($argument == \'current\') {
  $version = tripal_api_site_get_version(\'views\');
}
else {
  $version = $argument;
}
$handler->argument = tripal_api_site_get_branch_id(\'views\', $version);
return TRUE;';
$handler->display->display_options['path'] = 'views/%/search';

  return $view;
}